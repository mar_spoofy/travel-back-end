﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TravelAPI.Models;
using System.Web.Http.Cors;
using System.Web;
using Newtonsoft.Json.Linq;
using RestSharp;
using RestSharp.Extensions;
using SimpleJson;
using System.Globalization;

namespace TravelAPI.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class SearchController : ApiController
    {
        [HttpPost]
        public SearchResult GetOffers([FromBody]SearchData searchData)
        {

            BddContext bdd = new BddContext();
            List<Trip> trips = bdd.Trips.ToList();
            List<Trip> selectedTrips = new List<Trip>();
            List<TripDetail> tripsDetails = new List<TripDetail>();

            foreach (var trip in trips)
            {
                string[] stations = trip.Stations.Split(',');
                if (stations.Contains(searchData.DepartureLocationId.ToString()) && stations.Contains(searchData.ArrivalLocationId.ToString()))
                {
                    selectedTrips.Add(trip);
                }
            }

            foreach (var trip in selectedTrips)
            {
                TripDetail departure = bdd.TripDetails.SqlQuery("SELECT * FROM TripsDetails WHERE TripId=" + trip.Id + " AND DepartureLocationId=" + searchData.DepartureLocationId).FirstOrDefault();
                TripDetail arrival = bdd.TripDetails.SqlQuery("SELECT * FROM TripsDetails WHERE TripId=" + trip.Id + " AND ArrivalLocationId=" + searchData.ArrivalLocationId).FirstOrDefault();

                if (departure == null || arrival == null)
                    continue;

                if (DateTime.Compare(DateTime.Parse(searchData.DepartureTime), DateTime.Parse(departure.DepartureTime)) <= 0) {

                    List<TripDetail> l = bdd.TripDetails.SqlQuery("SELECT * FROM TripsDetails WHERE Id>=" + departure.Id + " AND Id<=" + arrival.Id + " ORDER BY Id ASC").ToList();

                    foreach (var item in l)
                    {
                        item.DepartureLocation = bdd.Cities.Find(item.DepartureLocationId);
                        item.ArrivalLocation = bdd.Cities.Find(item.ArrivalLocationId);
                        tripsDetails.Add(item);
                    }

                } else
                {
                    continue;
                }
            }

            // Get CTM Offers
            CTMDal ctmDal = new CTMDal();
            List<Voyage> voyages = new List<Voyage>();

            string[] dateElements = searchData.DepartureDate.Split('-');

            int d;
            int m;
            int y;

            if (int.TryParse(dateElements[0], out d))
            {
                // It was assigned. 
            }

            if (int.TryParse(dateElements[1], out m))
            {
                // It was assigned. 
            }

            if (int.TryParse(dateElements[2], out y))
            {
                // It was assigned. 
            }

            IRestResponse ctmResponse = ctmDal.GetCTMOffers(searchData.DepartureDate, searchData.DepartureLocationCode, searchData.ArrivalLocationCode);
            JObject resultJSon = JObject.Parse(ctmResponse.Content);
            JArray rows = (JArray)(resultJSon["rows"]);

            try
            {
                foreach (var r in rows)
                {
                    JArray cells = (JArray)r["cell"];

                    Voyage voyage = new Voyage();

                    voyage.Arrival = new City(searchData.ArrivalLocationCode, ctmDal.getCityName(int.Parse(searchData.ArrivalLocationCode)));
                    voyage.Depart = new City(searchData.DepartureLocationCode, ctmDal.getCityName(int.Parse(searchData.DepartureLocationCode)));
                    voyage.Company = new Company("CTM");

                    for (var i = 2; i < cells.Count; i++)
                    {
                        string tmp = (string)cells[i];

                        if (i == 2)
                        {
                            //DateTime tmpD = new DateTime(y, m, d);
                            //voyage.DepartTime = DateTime.Parse(tmpD.ToString("yyyy/MM/dd") + " " + tmp.Split(" ".ToCharArray())[0]);
                            voyage.DepartTime = tmp;
                        }
                        else
                        if (i == 3)
                        {
                            //price
                            voyage.Price = Double.Parse(tmp, CultureInfo.InvariantCulture);
                        }
                        else if (i == 4)
                        {
                            if (tmp.Equals(""))
                            {
                                voyage.Type = "CONFORT";
                            }
                            else
                            {
                                voyage.Type = tmp;
                            }
                        }
                        else if (i == 5)
                        {
                            string[] els = tmp.Split(" ".ToCharArray());
                            if (els.Length >= 2)
                            {
                                if (els[els.Length - 1].Equals("Lendemain"))
                                {
                                    DateTime tmpD = new DateTime(y, m, d);
                                    tmpD = tmpD.AddDays(1);
                                    // voyage.ArrivalTime = DateTime.Parse(tmpD.ToString("yyyy/MM/dd") + " " + els[0]);
                                    voyage.ArrivalTime = tmp;
                                }
                                else
                                {
                                    DateTime tmpD = new DateTime(y, m, d);
                                    // voyage.ArrivalTime = DateTime.Parse(tmpD.ToString("yyyy/MM/dd") + " " + els[0]);
                                    voyage.ArrivalTime = tmp;
                                }
                            }
                        }

                    }

                    voyages.Add(voyage);

                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return new SearchResult(tripsDetails, voyages);
            }

            return new SearchResult(tripsDetails, voyages);

        }
        
    }
}
