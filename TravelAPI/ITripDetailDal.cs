﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TravelAPI.Models;

namespace TravelAPI
{
    interface ITripDetailDal : IDisposable
    {
        void AddTripDetail(TripDetail tripDetail);
        TripDetail GetTripDetail(int id);
        List<TripDetail> GetAllTripsDetails();
    }
}
