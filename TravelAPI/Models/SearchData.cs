﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TravelAPI.Models
{
    public class SearchData
    {
        public int DepartureLocationId { get; set; }
        public int ArrivalLocationId { get; set; }
        public string DepartureLocationCode { get; set; }
        public string ArrivalLocationCode { get; set; }
        public string DepartureDate { get; set; }
        public string DepartureTime { get; set; }
    }
}