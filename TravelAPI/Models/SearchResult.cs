﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TravelAPI.Models
{
    public class SearchResult
    {

        public SearchResult(List<TripDetail> tripDetails, List<Voyage> voyages)
        {
            this.TripDetails = tripDetails;
            this.Voyages = voyages;
        }

        public List<TripDetail> TripDetails { get; set; }
        public List<Voyage> Voyages { get; set; }
    }
}